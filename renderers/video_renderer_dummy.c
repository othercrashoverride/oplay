/**
 * RPiPlay - An open-source AirPlay mirroring server for Raspberry Pi
 * Copyright (C) 2019 Florian Draschbacher
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

#include "video_renderer.h"

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>

#include "h264-bitstream/h264_stream.h"

#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/fb.h>
#include <sys/mman.h>

#include "amlogic.h"

const unsigned long PTS_FREQ = 90000;

const long EXTERNAL_PTS = (1);
const long SYNC_OUTSIDE = (2);
const long USE_IDR_FRAMERATE = 0x04;
const long UCODE_IP_ONLY_PARAM = 0x08;
const long MAX_REFER_BUF = 0x10;
const long ERROR_RECOVERY_MODE_IN = 0x20;

const char* CODEC_VIDEO_ES_DEVICE = "/dev/amstream_vbuf";
const char* CODEC_CNTL_DEVICE = "/dev/amvideo";

const unsigned int CLEAR_COLOR = 0xff101010;

// echo 1 | sudo tee /sys/class/video/freerun_mode

// /sys/class/video/screen_mode
// 0:normal
// 1:full stretch
// 2:4-3
// 3:16-9
// 4:non-linear
// 5:normal-noscaleup
// 6:4-3 ignore
// 7:4-3 letter box
// 8:4-3 pan scan
// 9:4-3 combined
// 10:16-9 ignore
// 11:16-9 letter box
// 12:16-9 pan scan
// 13:16-9 combined
// 14:Custom AR
// 15:AFD



typedef struct video_renderer_dummy_s {
    video_renderer_t base;
    int handle;
    int cntl_handle;
    int isActive;
    uint64_t firstPts;
    uint64_t clientStartTime;
} video_renderer_dummy_t;

static const video_renderer_funcs_t video_renderer_dummy_funcs;

video_renderer_t *video_renderer_dummy_init(logger_t *logger, video_renderer_config_t const *config) {
    video_renderer_dummy_t *renderer;
    renderer = calloc(1, sizeof(video_renderer_dummy_t));
    if (!renderer) {
        return NULL;
    }
    renderer->base.logger = logger;
    renderer->base.funcs = &video_renderer_dummy_funcs;
    renderer->base.type = VIDEO_RENDERER_DUMMY;


    renderer->handle = open(CODEC_VIDEO_ES_DEVICE, O_RDWR);
    if (renderer->handle < 0)
	{
        printf("Video codec open failed.\n");
		abort();
	}


	// Set video format
	struct am_ioctl_parm parm;
    memset(&parm, 0, sizeof(parm));

	int r;

    parm.cmd = AMSTREAM_SET_VFORMAT;
    parm.data_vformat = VFORMAT_H264;

    r = ioctl(renderer->handle, AMSTREAM_IOC_SET, (unsigned long)&parm);
    if (r < 0)
    {
        printf("AMSTREAM_IOC_SET failed.\n");
        abort();
    }


	dec_sysinfo_t am_sysinfo = { 0 };
	am_sysinfo.param = (void*)(EXTERNAL_PTS);
	am_sysinfo.rate = 96000.0 / 60 + 1;
    am_sysinfo.format = VIDEO_DEC_FORMAT_H264;

	r = ioctl(renderer->handle, AMSTREAM_IOC_SYSINFO, (unsigned long)&am_sysinfo);
	if (r < 0)
	{
		printf("AMSTREAM_IOC_SYSINFO failed.\n");
        abort();
	}


    memset(&parm, 0, sizeof(parm));
    parm.cmd = AMSTREAM_PORT_INIT;

    r = ioctl(renderer->handle, AMSTREAM_IOC_SET, (unsigned long)&parm);
    if (r != 0)
    {
        printf("AMSTREAM_PORT_INIT failed.\n");
        abort();
    }


	// Control device
	renderer->cntl_handle = open(CODEC_CNTL_DEVICE, O_RDWR);
	if (renderer->cntl_handle < 0)
	{
		printf("open CODEC_CNTL_DEVICE failed.\n");
        abort();
	}


	r = ioctl(renderer->cntl_handle, AMSTREAM_IOC_SYNCENABLE, (unsigned long)1);
	if (r != 0)
	{
		printf("AMSTREAM_IOC_SYNCENABLE failed.\n");
        abort();
	}


    r = ioctl(renderer->cntl_handle, AMSTREAM_IOC_SET_FREERUN_MODE, (unsigned long)config->low_latency ? FREERUN_NODUR : FREERUN_NONE);
	if (r < 0)
	{
		printf("AMSTREAM_SET_FREERUN_MODE failed.\n");
        abort();
	}

    int arg = 0;
    r = ioctl(renderer->cntl_handle, AMSTREAM_IOC_SET_SCREEN_MODE, (unsigned long)&arg);
	if (r < 0)
	{
		printf("AMSTREAM_IOC_SET_SCREEN_MODE failed. (%d)\n", r);
        abort();
	}


    int fd = open("/dev/fb0", O_RDWR);

    // enable alpha setting
    struct fb_var_screeninfo info;
	if (ioctl(fd, FBIOGET_VSCREENINFO, &info) < 0)
    {
        printf("FBIOGET_VSCREENINFO failed.\n");
        abort();
    }

	info.reserved[0] = 0;
	info.reserved[1] = 0;
	info.reserved[2] = 0;
	info.xoffset = 0;
	info.yoffset = 0;
	info.activate = FB_ACTIVATE_NOW;
	info.red.offset = 16;
	info.red.length = 8;
	info.green.offset = 8;
	info.green.length = 8;
	info.blue.offset = 0;
	info.blue.length = 8;
	info.transp.offset = 24;
	info.transp.length = 8;
	info.nonstd = 1;

	if (ioctl(fd, FBIOPUT_VSCREENINFO, &info) < 0)
    {
        printf("FBIOPUT_VSCREENINFO failed.\n");
        abort();
    }


    // Clear the screen
    struct fb_fix_screeninfo fixed_info;
    if (ioctl(fd, FBIOGET_FSCREENINFO, &fixed_info) < 0)
    {
        printf("FBIOGET_FSCREENINFO failed.\n");
        abort();
    }

    unsigned int* framebuffer = (unsigned int*)mmap(NULL,
        fixed_info.smem_len,
        PROT_READ | PROT_WRITE,
        MAP_SHARED,
        fd,
        0);

    for (int i = 0; i < fixed_info.smem_len / 4; ++i)
    {
        framebuffer[i] = CLEAR_COLOR;
    }

    munmap(framebuffer, fixed_info.smem_len);

    close(fd);

    renderer->isActive = 0;
    renderer->firstPts = 0;

    return &renderer->base;
}

static void video_renderer_dummy_start(video_renderer_t *renderer) {
}

static void video_renderer_dummy_render_buffer(video_renderer_t *renderer, raop_ntp_t *ntp, unsigned char *data, int data_len, uint64_t pts, int type) {
    video_renderer_dummy_t* r = (video_renderer_dummy_t*)renderer;

    if (r->firstPts < 1 && pts > 0)
    {
        r->clientStartTime = ((int64_t) raop_ntp_get_local_time(ntp));;
        r->firstPts = pts;
    }

    if (!r->isActive)
    {
        int fd = open("/dev/fb0", O_RDWR);

        // Clear the screen
        struct fb_fix_screeninfo fixed_info;
        if (ioctl(fd, FBIOGET_FSCREENINFO, &fixed_info) < 0)
        {
            printf("FBIOGET_FSCREENINFO failed.\n");
            abort();
        }

        unsigned int* framebuffer = (unsigned int*)mmap(NULL,
            fixed_info.smem_len,
            PROT_READ | PROT_WRITE,
            MAP_SHARED,
            fd,
            0);

        memset(framebuffer, 0, fixed_info.smem_len);

        munmap(framebuffer, fixed_info.smem_len);

        close(fd);

        r->isActive = 1;
    }


    const unsigned long PTS_FREQ = 90000;

    uint64_t client_time = raop_ntp_get_local_time(ntp);
    uint64_t client_pts = client_time - r->clientStartTime;
    uint32_t aml_client_pts = client_pts * 0.000001 * PTS_FREQ;

    int64_t video_pts = pts - r->firstPts;
    uint32_t aml_video_pts = video_pts * 0.000001 * PTS_FREQ;
 
    //printf("PTS=%ld (%ld) diff=%ld\n", video_pts, client_pts, video_pts - client_pts);

#if 1
    struct am_ioctl_parm parm = { 0 };

    parm.cmd = AMSTREAM_SET_PCRSCR;
    parm.data_32 = (aml_client_pts);
    //parm.data_64 = (unsigned long)(value * PTS_FREQ);

    int io = ioctl(r->handle, AMSTREAM_IOC_SET, (unsigned long)&parm);
    if (io < 0)
    {
        printf("AMSTREAM_SET_PCRSCR failed.");
        abort();
    }


    //parm = { 0 };
    parm.cmd = AMSTREAM_SET_TSTAMP;
    parm.data_32 = aml_video_pts;

    io = ioctl(r->handle, AMSTREAM_IOC_SET, (unsigned long)&parm);
    if (io < 0)
    {
        printf("AMSTREAM_SET_TSTAMP failed\n");
        abort();
    }
#endif

    uint8_t *modified_data = NULL;
#if 1
    if (type == 0) {
        logger_log(renderer->logger, LOGGER_DEBUG, "Injecting max_dec_frame_buffering");
        int sps_start, sps_end;
        int sps_size = find_nal_unit(data, data_len, &sps_start, &sps_end);
        if (sps_size > 0) {
            const int sps_wiggle_room = 12;
            const unsigned char nal_marker[] = { 0x0, 0x0, 0x0, 0x1 };
            int modified_data_len = data_len + sps_wiggle_room + sizeof(nal_marker);
            modified_data = malloc(modified_data_len);

            h264_stream_t *h = h264_new();
            h->nal->nal_unit_type = NAL_UNIT_TYPE_SPS;
            h->sps->vui.bitstream_restriction_flag = 1;
            h->sps->vui.max_dec_frame_buffering = 4;

            int new_sps_size = write_nal_unit(h, modified_data + sps_start, sps_wiggle_room);
            if (new_sps_size > 0 && new_sps_size <= sps_wiggle_room) {
                memcpy(modified_data, data, sps_start);
                memcpy(modified_data + sps_start + new_sps_size, nal_marker, sizeof(nal_marker));
                memcpy(modified_data + sps_start + new_sps_size + sizeof(nal_marker), data + sps_start, data_len - sps_start);
                data = modified_data;
                data_len = data_len + new_sps_size + sizeof(nal_marker);
            } else {
                free(modified_data);
                modified_data = NULL;
            }
            h264_free(h);
        } else {
            logger_log(renderer->logger, LOGGER_ERR, "Could not find sps boundaries");
        }
    }
#endif

    int maxAttempts = 150;
	int offset = 0;
	while (offset < data_len)
	{
		int count = write(r->handle, data + offset, data_len - offset);
		if (count > 0)
		{
			offset += count;
			//printf("codec_write send %x bytes of %x total.\n", count, pkt.size);
		}
		else
		{
			//printf("codec_write failed (%x).\n", count);
			maxAttempts -= 1;

			if (maxAttempts <= 0)
			{
				printf("codec write max attempts exceeded.\n");
				break;
			}
		}
	}

    if (modified_data) {
        // We overwrote the data buffer to inject the max_dec_frame_buffering before,
        // so we need to free the data buffer here
        free(modified_data);
    }
}

static void video_renderer_dummy_flush(video_renderer_t *renderer) {
    video_renderer_dummy_t* r = (video_renderer_dummy_t*)renderer;
    int fd = open("/dev/fb0", O_RDWR);
    
    // Clear the screen
    struct fb_fix_screeninfo fixed_info;
    if (ioctl(fd, FBIOGET_FSCREENINFO, &fixed_info) < 0)
    {
        printf("FBIOGET_FSCREENINFO failed.\n");
        abort();
    }

    unsigned int* framebuffer = (unsigned int*)mmap(NULL,
        fixed_info.smem_len,
        PROT_READ | PROT_WRITE,
        MAP_SHARED,
        fd,
        0);

    for (int i = 0; i < fixed_info.smem_len / 4; ++i)
    {
        framebuffer[i] = CLEAR_COLOR;
    }

    munmap(framebuffer, fixed_info.smem_len);

    close(fd);


    r->isActive = 0;
}

static void video_renderer_dummy_destroy(video_renderer_t *renderer) {
    video_renderer_dummy_t* r = (video_renderer_dummy_t*)renderer;

    close(r->cntl_handle);
    close(r->handle);

    if (renderer) {
        free(renderer);
    }
}

static void video_renderer_dummy_update_background(video_renderer_t *renderer, int type) {

}

static const video_renderer_funcs_t video_renderer_dummy_funcs = {
    .start = video_renderer_dummy_start,
    .render_buffer = video_renderer_dummy_render_buffer,
    .flush = video_renderer_dummy_flush,
    .destroy = video_renderer_dummy_destroy,
    .update_background = video_renderer_dummy_update_background,
};
