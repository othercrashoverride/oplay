/**
 * RPiPlay - An open-source AirPlay mirroring server for Raspberry Pi
 * Copyright (C) 2019 Florian Draschbacher
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */

#include "audio_renderer.h"

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>

#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alext.h>

#include "fdk-aac/libAACdec/include/aacdecoder_lib.h"


#define FREQUENCY (44100)

typedef struct audio_renderer_dummy_s {
    audio_renderer_t base;
    ALCdevice* device;
    ALCcontext* context;
    ALuint source;
    int frequency;

    HANDLE_AACDECODER audio_decoder;
    INT time_data_size;
    INT_PCM* p_time_data;
} audio_renderer_dummy_t;

static const audio_renderer_funcs_t audio_renderer_dummy_funcs;

audio_renderer_t *audio_renderer_dummy_init(logger_t *logger, video_renderer_t *video_renderer, audio_renderer_config_t const *config) {
    audio_renderer_dummy_t *renderer;
    renderer = calloc(1, sizeof(audio_renderer_dummy_t));
    if (!renderer) {
        return NULL;
    }
    renderer->base.logger = logger;
    renderer->base.funcs = &audio_renderer_dummy_funcs;
    renderer->base.type = AUDIO_RENDERER_DUMMY;


    renderer->frequency = FREQUENCY;

    renderer->device = alcOpenDevice(NULL);
	if (!renderer->device)
	{
		printf("alcOpenDevice failed.\n");
		abort();
	}

	renderer->context = alcCreateContext(renderer->device, NULL);
	if (!alcMakeContextCurrent(renderer->context))
	{
		printf("alcMakeContextCurrent failed.\n");
		abort();
	}

	alGenSources((ALuint)1, &renderer->source);

	alSourcef(renderer->source, AL_PITCH, 1);
	alSourcef(renderer->source, AL_GAIN, 1);
	alSource3f(renderer->source, AL_POSITION, 0, 0, 0);
	alSource3f(renderer->source, AL_VELOCITY, 0, 0, 0);
	alSourcei(renderer->source, AL_LOOPING, AL_FALSE);


    // Each buffer is 480 frames of audio:    
    //  480 / 44100 = 0.010884354 s
    //  1 / 60 = 0.016666667s
    //  0.016666667 / 0.010884354 = 1.531249964 audio buffers per video frame
	const int BUFFER_COUNT = 8;
	for (int i = 0; i < BUFFER_COUNT; ++i)
	{
		ALuint buffer;
		alGenBuffers((ALuint)1, &buffer);
		alBufferData(buffer, AL_FORMAT_STEREO16, NULL, 0, renderer->frequency);
		alSourceQueueBuffers(renderer->source, 1, &buffer);
	}

	alSourcePlay(renderer->source);



    renderer->audio_decoder = aacDecoder_Open(TT_MP4_RAW, 1);
    if (renderer->audio_decoder == NULL) {
        logger_log(renderer->base.logger, LOGGER_ERR, "aacDecoder open faild!");
        abort();
    }

    /* ASC config binary data */
    UCHAR eld_conf[] = { 0xF8, 0xE8, 0x50, 0x00 };
    UCHAR *conf[] = { eld_conf };
    static UINT conf_len = sizeof(eld_conf);
    int ret = aacDecoder_ConfigRaw(renderer->audio_decoder, conf, &conf_len);
    if (ret != AAC_DEC_OK) {
        logger_log(renderer->base.logger, LOGGER_ERR, "Unable to set configRaw");
        abort();
    }
    CStreamInfo *aac_stream_info = aacDecoder_GetStreamInfo(renderer->audio_decoder);
    if (aac_stream_info == NULL) {
        logger_log(renderer->base.logger, LOGGER_ERR, "aacDecoder_GetStreamInfo failed!");
        abort();
    }

    renderer->time_data_size = 480 * 2 * sizeof(short); //4 * 480;
    renderer->p_time_data = malloc(renderer->time_data_size); // The buffer for the decoded AAC frames
    if (!renderer->p_time_data)
    {
        printf("malloc failed.\n");
        abort();
    }

    logger_log(renderer->base.logger, LOGGER_DEBUG, "> stream info: channel = %d\tsample_rate = %d\tframe_size = %d\taot = %d\tbitrate = %d",   \
            aac_stream_info->channelConfig, aac_stream_info->aacSampleRate,
            aac_stream_info->aacSamplesPerFrame, aac_stream_info->aot, aac_stream_info->bitRate);


    return &renderer->base;
}

static void audio_renderer_dummy_start(audio_renderer_t *renderer) {
    audio_renderer_dummy_t* r = (audio_renderer_dummy_t*)renderer;

	alSourcePlay(r->source);
}

static void audio_renderer_dummy_render_buffer(audio_renderer_t *renderer, raop_ntp_t *ntp, unsigned char *data, int data_len, uint64_t pts) {
    audio_renderer_dummy_t* r = (audio_renderer_dummy_t*)renderer;

    // We assume that every buffer contains exactly 1 frame.

    AAC_DECODER_ERROR error = 0;

    UCHAR *p_buffer[1] = {data};
    UINT buffer_size = data_len;
    UINT bytes_valid = data_len;
    error = aacDecoder_Fill(r->audio_decoder, p_buffer, &buffer_size, &bytes_valid);
    if (error != AAC_DEC_OK) {
        logger_log(renderer->logger, LOGGER_ERR, "aacDecoder_Fill error : %x", error);
    }

    // INT time_data_size = 4 * 480;
    // INT_PCM *p_time_data = malloc(time_data_size); // The buffer for the decoded AAC frames
    error = aacDecoder_DecodeFrame(r->audio_decoder, r->p_time_data, r->time_data_size, 0);
    if (error != AAC_DEC_OK) {
        logger_log(renderer->logger, LOGGER_ERR, "aacDecoder_DecodeFrame error : 0x%x", error);
    }

#ifdef DUMP_AUDIO
    if (file_pcm == NULL) {
        file_pcm = fopen("/home/pi/Airplay.pcm", "wb");
    }

    fwrite(p_time_data, time_data_size, 1, file_pcm);
#endif

    // uint64_t client_time = raop_ntp_get_local_time(ntp);
    // uint64_t client_pts = client_time - r->clientStartTime;
    // uint32_t client_seconds = client_pts * 0.000001;

    if (!alcMakeContextCurrent(r->context))
    {
        printf("alcMakeContextCurrent failed.\n");
        abort();
    }

    ALint processed = 0;
    while(!processed)
    {
        alGetSourceiv(r->source, AL_BUFFERS_PROCESSED, &processed);

        if (!processed)
        {
            //printf("Audio overflow.\n");
            //sleep(0);
            
            goto out;
        }
    }

    ALuint openALBufferID;
    alSourceUnqueueBuffers(r->source, 1, &openALBufferID);

    const ALuint format = AL_FORMAT_STEREO16;
    alBufferData(openALBufferID, format, r->p_time_data, r->time_data_size, r->frequency);

    alSourceQueueBuffers(r->source, 1, &openALBufferID);

    ALint result;
    alGetSourcei(r->source, AL_SOURCE_STATE, &result);
    if (result != AL_PLAYING)
    {
        alSourcePlay(r->source);
    }

out:
    return;
}

static void audio_renderer_dummy_set_volume(audio_renderer_t *renderer, float volume) {
}

static void audio_renderer_dummy_flush(audio_renderer_t *renderer) {
}

static void audio_renderer_dummy_destroy(audio_renderer_t *renderer) {
    if (renderer) {
        audio_renderer_dummy_t* r = (audio_renderer_dummy_t*)renderer;
        free(r->p_time_data);

        free(renderer);
    }
}

static const audio_renderer_funcs_t audio_renderer_dummy_funcs = {
    .start = audio_renderer_dummy_start,
    .render_buffer = audio_renderer_dummy_render_buffer,
    .set_volume = audio_renderer_dummy_set_volume,
    .flush = audio_renderer_dummy_flush,
    .destroy = audio_renderer_dummy_destroy,
};
